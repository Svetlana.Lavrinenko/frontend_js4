"use strict";

const personalMovieDB = {
    count: 0,
    movies: {},
    actors: {},
    genres: [],
    privat: false,

    start: function () {
        personalMovieDB.count = +prompt("Сколько фильмов вы уже посмотрели?");

        while (personalMovieDB.count == '' || personalMovieDB.count == null || isNaN(personalMovieDB.count)) {
            personalMovieDB.count = +prompt("Сколько фильмов вы уже посмотрели?");
        }

    },

    myFilms: function () {

        for (let i = 0; i < 2; i++) {
            const quetion = prompt("Один из последних просмотренных фильмов?");
            const quetionRaiting = +prompt("На сколько оцените его?");

            if (quetion != null && quetion.length < 50 && quetion != "" && quetionRaiting != null && quetionRaiting != "") {

                personalMovieDB.movies[quetion] = quetionRaiting;
                console.log('Done');
            }
            else {
                console.log('Error');
                i--;
                continue;
            }

        }

    },


    filmsCount: function () {
        if (personalMovieDB.count < 10) { console.log("Просмотрено довольно мало фильмов"); }
        else if (personalMovieDB.count > 10 && personalMovieDB.count < 30) { console.log("Вы классический зритель"); }
        else if (personalMovieDB.count > 30) { console.log("Вы киноман"); }
        else { console.log("Произошла ошибка"); }
    },


    showMyDB: function () {

        if (personalMovieDB.privat == false) {
            console.log(personalMovieDB);
        }

    },


    toggleVisibleMyDB: function () {
        if (personalMovieDB.privat == false) {
            personalMovieDB.privat = true;
        }
        else {
            personalMovieDB.privat = false;
        }
    },

    writeYourGenres: function () {
        for (let i = 1; i <= 3; i++) {
            let genre = prompt(`Ваш любимый жанр номер ${i}`);

            if (genre == "" || genre == null) {
                console.log("пустое поле или некорректные данные")
                i--;

            }
            else {
                personalMovieDB.genres[i - 1] = genre;
            }
        }
        personalMovieDB.genres.forEach((item, i, genres) => {
            console.log(`Любимый жанр ${i + 1} - это ${item}`);
        });

    }

};